
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define INFINITY 9999

#define LIMIT pow(2, 10)

double get_ex_time(const struct timespec b_time, const struct timespec e_time) {
  return (e_time.tv_sec - b_time.tv_sec) +
         (e_time.tv_nsec - b_time.tv_nsec) / 1E9;
}
void dijkstra(int **G, int n) {

  int count, mindistance, nextnode, i, j;
  int **cost = (int **)malloc(n * sizeof(int *));
  for (int index = 0; index < n; ++index) {
    cost[index] = (int *)malloc(n * sizeof(int));
  }
  int *distance = (int *)malloc(n * sizeof(int));
  int *pred = (int *)malloc(n * sizeof(int));
  int *visited = (int *)malloc(n * sizeof(int));

  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      if (G[i][j] == 0)
        cost[i][j] = INFINITY;
      else
        cost[i][j] = G[i][j];
    }
  }
  for (i = 0; i < n; i++) {
    distance[i] = cost[0][i];
    pred[i] = 0;
    visited[i] = 0;
  }
  distance[0] = 0;
  visited[0] = 1;
  count = 1;
  while (count < n - 1) {
    mindistance = INFINITY;

    // nextnode gives the node at minimum distance
    for (i = 0; i < n; i++)
      if (distance[i] < mindistance && !visited[i]) {
        mindistance = distance[i];
        nextnode = i;
      }

    // check if a better path exists through nextnode
    visited[nextnode] = 1;
    for (i = 0; i < n; i++)
      if (!visited[i])
        if (mindistance + cost[nextnode][i] < distance[i]) {
          distance[i] = mindistance + cost[nextnode][i];
          pred[i] = nextnode;
        }
    count++;
  }
  /*for (i = 0; i < n; i++)
    if (i != 0) {
      printf("\nDistance of node%d=%d", i, distance[i]);
      printf("\nPath=%d", i);

      j = i;
      do {
        j = pred[j];
        printf("<-%d", j);
      } while (j != 0);
    }*/
  for (int index = 0; index < n; ++index) {
    free(cost[index]);
  }
  free(cost);
  free(pred);
  free(visited);
  free(distance);
}

int main() {
  struct timespec b_time, e_time;
  for (int j = 4; j < LIMIT; j = j * 2) {
    printf("%d", j);
    int **graph = (int **)malloc(j * sizeof(int *));
    for (int i = 0; i < j; i++)
      graph[i] = (int *)malloc(j * sizeof(int));

    for (int i = 0; i < j; i++) {
      for (int k = 0; k < j; k++) {
        if (i != k)
          graph[i][k] = (rand() / 10000000) % 5;
      }
    }

    /*for (int i = 0; i < j; i++) {
      for (int k = 0; k < j; k++) {
        printf("%d ", graph[i][k]);
      }
      printf("\n");
    }*/
    clock_gettime(CLOCK_REALTIME, &b_time);
    dijkstra(graph, j);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf(" %lf", get_ex_time(b_time, e_time));
    printf("\n");
    free(graph);
  }
  return 0;
}
