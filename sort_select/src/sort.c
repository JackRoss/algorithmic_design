#include <math.h> //used in bucket sort
#include <stdio.h>
#include <stdlib.h>
void swopd(double *a, double *b) {
  if (*a == *b) {
    return;
  }
  *a = *a + *b;
  *b = *a - *b;
  *a = *a - *b;
}

void swop(int *a, int *b) {
  if (*a == *b) {
    return;
  }
  *a = *a + *b;
  *b = *a - *b;
  *a = *a - *b;
}
int part(int *v, int left, int r, int p) {
  int i, j;
  swop(&v[left], &v[p]);
  i = left + 1;
  j = r;
  while (i <= j) {
    if (v[i] <= v[left]) {
      i += 1;
    } else if (v[j] > v[left]) {
      j -= 1;
    } else {
      swop(&v[i], &v[j]);
      i++;
      j--;
    }
  }
  swop(&v[j], &v[left]);
  return j;
}

void quick_sort(int *v, int l, int r) {
  int p;
  while (l < r) {
    p = part(v, l, r, l);
    quick_sort(v, l, p - 1);
    l = p + 1;
  }
}
// quicksort rhat take the element in the middle as pivot
void quick_sort1(int *v, int left, int r) {
  int p;
  while (left < r) {
    p = part(v, left, r, (r + left) / 2);
    quick_sort1(v, left, p - 1);
    left = p + 1;
  }
}
int partd(double *v, int left, int r, int p) {
  int i, j;
  i = left + 1;
  j = r;
  while (i <= j) {
    if (v[i] <= v[left]) {
      i += 1;
    } else if (v[j] > v[left]) {
      j -= 1;
    } else {
      swopd(&v[i], &v[j]);
      i++;
      j--;
    }
  }
  swopd(&v[j], &v[left]);
  return j;
}

void quick_sortd(double *v, int left, int r) {
  int p;
  while (left < r) {
    p = partd(v, left, r, left);
    quick_sortd(v, left, p - 1);
    left = p + 1;
  }
}

int *counting_sort(int *items, const size_t n, const int min, const int max) {

  int k = max - min + 1;
  int *b = (int *)calloc(n, sizeof(int));
  size_t *c = (size_t *)calloc(k, sizeof(size_t));
  for (int i = 0; i < k; ++i)
    c[i] = 0;

  for (size_t i = 0; i < n; ++i)
    c[(items[i] - min)]++;

  for (int i = 1; i < k; ++i)
    c[i] += c[i - 1];

  for (size_t j = n - 1; j > 0; --j)
    b[--c[(items[j] - min)]] = items[j];
  b[--c[(items[0] - min)]] = items[0];
  return b;
  free(c);
}

void bucket_sort(double *items, size_t n) {

  double **buckets = (double **)calloc(n, sizeof(double *));
  size_t *buckets_size = (size_t *)calloc(n, sizeof(size_t));
  for (size_t i = 0; i < n; ++i) {
    buckets[i] = (double *)calloc(n, sizeof(double));
    buckets_size[i] = 0;
  }

  // fill buckets
  for (size_t i = 0; i < n; ++i) {

    size_t bucket_id = (size_t)floor(items[i] * n);
    buckets[bucket_id][buckets_size[bucket_id]] = items[i];
    buckets_size[bucket_id] += 1;
  }

  // sort buckets
  for (size_t i = 0; i < n; ++i)
    quick_sortd(&(buckets[i][0]), 0, buckets_size[i]);

  size_t pos = 0;
  for (size_t i = 0; i < n; ++i) {

    for (size_t j = 0; j < buckets_size[i]; ++j) {
      items[pos] = buckets[i][j];
      ++pos;
    }
  }

  for (size_t i = 0; i < n; ++i) {
    free(buckets[i]);
  }
  free(buckets);
  free(buckets_size);
}

void insert(int *a, int N) {
  int tmp;
  for (int i = 1; i <= N; i++) {
    tmp = a[i];
    int j = i;
    while (a[j - 1] > tmp) {
      swop(&a[j], &a[j - 1]);
      j--;
    }
    a[j] = tmp;
  }
}

int max(int arr[], int n) {
  int mx = arr[0];
  for (int i = 1; i < n; i++)
    if (arr[i] > mx)
      mx = arr[i];
  return mx;
}

void radsort(int A[], int n) {
  int i, digitPlace = 1;
  int result[n]; // resulting array (as in counting sort)
  // Find the largest number to know number of digits
  int largestNum = max(A, n);

  // we run loop until we reach the largest digit place
  while (largestNum / digitPlace != 0) {

    int keys[10] = {0};
    // Store keys or digits
    for (i = 0; i < n; i++)
      keys[(A[i] / digitPlace) % 10]++;

    for (i = 1; i < 10; i++)
      keys[i] += keys[i - 1];

    // Build the resulting array
    for (i = n - 1; i >= 0; i--) {
      result[keys[(A[i] / digitPlace) % 10] - 1] = A[i];
      keys[(A[i] / digitPlace) % 10]--;
    }

    // Now main array A[] contains sorted numbers
    for (i = 0; i < n; i++)
      A[i] = result[i];

    // Move to next digit place
    digitPlace *= 10;
  }
}
