
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int left(int i) { return (2 * i + 1); }

int right(int i) { return (2 * i + 2); }

int parent(int i) { return floor((i - 1) / 2); }

int get_root() { return 1; }

int is_valid(int *n, int i) { return *n >= i; }

void swap(int *x, int *y) {
  int temp = *x;
  *x = *y;
  *y = temp;
}

void heapify(int *heap, int i, int *n) {
  int m = i;

  if (left(i) < *n && heap[left(i)] > heap[m]) {

    m = left(i);
  }
  if (right(i) < *n && heap[right(i)] > heap[m]) {

    m = right(i);
  }

  if (i != m) {

    swap(&heap[i], &heap[m]);
    heapify(heap, m, n);
    // printf("entrato \n");
  }
}

void remove_min(int *heap, int *n) {
  heap[0] = heap[*n - 1];
  *n = *n - 1;
  heapify(heap, 0, n);
}

void build_heap(int *a, int *n) {

  for (int i = parent(*n - 1); i >= 0; i--) {

    heapify(a, i, n); // second i could be wrong
  }
}
void decrease(int *heap, int i, int val) {
  if (heap[i] >= val)
    printf("the chosen value is greater then the existing one\n");

  heap[i] = val;
  while (i != 0 || heap[parent(i)] < heap[i]) {
    swap(&heap[i], &heap[parent(i)]);

    i = parent(i);
  }
}

void heap_sort(int *A, int *n) {
  /*int *H = (int *)malloc(*n * sizeof(int));
  int m;
  m = *n;
  for (int i = 0; i < *n; i++) {
    H[i] = A[i];
  }
  */
  build_heap(A, n);
  for (int i = *n - 1; i > 0; i--) {
    swap(&A[0], &A[i]);
    ;
    heapify(A, 0, &i);
    // printf("%d \n\n", i);
  }
}
