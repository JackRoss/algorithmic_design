#include "sort.h"
#include <stdio.h>
#include <stdlib.h>
void swap1(int *A, int i, int j) {
  int tmp = A[i];
  A[i] = A[j];
  A[j] = tmp;
}

int partition(int *array, int b, int e,
              int pivot) { // b=first index; e=last index
  swap1(array, b, pivot);
  pivot = b;
  int right = 0;
  while (b <= e) {
    if (array[b] > array[pivot]) {
      swap1(array, b, e);

      e--;
    }

    else if (array[b] == array[pivot]) {
      if (right) {
        swap1(array, b, e);
        e--;
        right = 0;
      } else {
        b++;
        right = 1;
      }
    }

    else
      b++;
  }
  swap1(array, pivot, e);
  return e;
}
// select the approx median pivot

int select_pivot(int *array, int b, int e) {
  // find the number of chunks with dimension 5
  int chunks = (e - b) / 5 + 1;
  if (e - b + 1 <= 5) {
    quick_sort(array, b, e);
    return (b + e) / 2;
  }

  int cc = b + 4;

  while (cc < e - 5) {
    quick_sort(array, cc - 4, cc);
    if (cc + 5 >= e - 5)
      cc = e;
    else
      cc += 5;
  }

  int j = b + 5;
  for (int i = b; i < chunks; i++) {
    swap1(array, i, (j + i * 5) / 2);
    if (j + 5 >= e)
      j = e;
    else
      j += 5;
  }
  int res;
  res = select_pivot(array, b, b + chunks);
  return res;
}

int selecti(int *array, int i, int b, int e) {
  int j = select_pivot(array, b, e);
  int k = partition(array, b, e, j);

  if (i == k + 1)
    return array[k];
  if (i < k + 1)
    return selecti(array, i, b, k - 1);
  return selecti(array, i, k + 1, e);
}
