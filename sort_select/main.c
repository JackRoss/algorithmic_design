#include "sort.h"
#include "util.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define LIMIT 65537

int main() {
  struct timespec b_time, e_time;
  printf("#size #insertion #quick #counting  #radix #bucket\n");
  for (size_t i = 128; i < LIMIT; i *= 2) {
    printf("%ld ", i);
    int *array1 = (int *)malloc(sizeof(int) * i);
    int *array2 = (int *)malloc(sizeof(int) * i);
    int *array3 = (int *)malloc(sizeof(int) * i);
    int *array4 = (int *)malloc(sizeof(int) * i);
    int *array5 = (int *)malloc(sizeof(int) * i);
    double *array6 = (double *)malloc(sizeof(double) * i);

    int *result1 = (int *)malloc(sizeof(int) * i);

    random_fill_array(array1, i);
    random_fill_array(array2, i);
    random_fill_array(array3, i);
    random_fill_array(array4, i);
    random_fill_array(array5, i);
    random_fill_arrayD(array6, i);

    // print_array(array1,i);

    if (i < 65537) {
      clock_gettime(CLOCK_REALTIME, &b_time);
      insert(array1, i);
      clock_gettime(CLOCK_REALTIME, &e_time);
      printf("%lf", get_ex_time(b_time, e_time));
      // print_array(array1,i);
    }

    clock_gettime(CLOCK_REALTIME, &b_time);
    quick_sort(array2, 0, i - 1);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf(" %lf", get_ex_time(b_time, e_time));
    // print_array(array2,i);

    clock_gettime(CLOCK_REALTIME, &b_time);
    array3 = counting_sort(array4, i, 0, 510);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf(" %lf", get_ex_time(b_time, e_time));
    // print_array(result1,i);

    clock_gettime(CLOCK_REALTIME, &b_time);
    radsort(array5, i);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf(" %lf", get_ex_time(b_time, e_time));
    // print_array(result1,i);
    if (i < 65537) {

      clock_gettime(CLOCK_REALTIME, &b_time);
      bucket_sort(array6, i);
      clock_gettime(CLOCK_REALTIME, &e_time);
      printf(" %lf\n", get_ex_time(b_time, e_time));
    }

    free(array6);

    free(array1);
    free(array2);
    free(array3);
    free(array4);
    free(array5);

    free(result1);
  }
  return 0;
}
