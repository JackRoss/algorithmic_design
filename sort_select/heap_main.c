
#include "heap.h"
#include "util.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define LIMIT pow(2, 21)
int main() {
  struct timespec b_time, e_time;
  printf("#size #time\n");
  for (int i = 128; i < LIMIT; i *= 2) {
    printf("%d", i);
    int *array1 = (int *)malloc(sizeof(int) * i);
    random_fill_array(array1, i);
    clock_gettime(CLOCK_REALTIME, &b_time);
    heap_sort(array1, &i);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf(" %lf\n", get_ex_time(b_time, e_time));
    free(array1);
  }
  return 0;
}
