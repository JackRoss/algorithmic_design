#include <math.h>
#include <stdio.h>
#include <stdlib.h>
int *counting_sort(int *items, const size_t n, const int min, const int max);

void bucket_sort(double *items, size_t n);

void swopd(double *a, double *b);

void swop(int *a, int *b);

void insert(int *a, int N);

int part(int *v, int left, int r, int p);

void quick_sort(int *v, int left, int r);

void quick_sort1(int *v, int left, int r);

int partd(double *v, int left, int r, int p);

void quick_sortd(double *v, int left, int r);

int max(int arr[], int n);

void radsort(int A[], int n);
