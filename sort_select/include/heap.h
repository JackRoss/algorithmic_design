#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int left(int i);

int right(int i);

int parent(int i);

int get_root();

int is_valid(int *n, int i);

void swap(int *x, int *y);

void heapify(int *heap, int i, int *n);

void remove_min(int *heap, int *n);

void build_heap(int *a, int *n);

void decrease(int *heap, int i, int val);

void heap_sort(int *A, int *n);
