#include "select.h"
#include "util.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define LIMIT pow(2, 20)

int main() {
  struct timespec b_time, e_time;
  printf("#size #time\n");
  for (size_t i = 4; i < LIMIT; i *= 2) {

    int *array1 = (int *)malloc(sizeof(int) * i);

    // for(int j = 0; j < i ; j++)
    //    array1[j] = j+1;

    random_fill_array(array1, i);
    // print_array(array1,i);

    clock_gettime(CLOCK_REALTIME, &b_time);
    int selected = selecti(array1, i / 3 + 1, 0, i - 1);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf("%ld %lf\n", i, get_ex_time(b_time, e_time));

    free(array1);
  }
}
