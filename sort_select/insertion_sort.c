#include <stdio.h>
#include <stdlib.h>
void swop(int *a, int *b) {
  if (*a == *b) {
    return;
  }
  *a = *a + *b;
  *b = *a - *b;
  *a = *a - *b;
}

void insert(int *a, int N) {
  int tmp;
  for (int i = 1; i <= N; i++) {
    tmp = a[i];
    int j = i;
    while (a[j - 1] > tmp) {
      swop(&a[j], &a[j - 1]);
      j--;
    }
    a[j] = tmp;
  }
}

int main() {
  int n = 18;
  int *arr = (int *)malloc(sizeof(int) * n);
  for (int i = 0; i < n; i++) {
    arr[i] = rand() / 10000000;
    printf("%d ", arr[i]);
  }
  printf("\n______________________________\n");
  insert(arr, n);

  for (int i = 0; i < n; i++)
    printf("%d ", arr[i]);
  printf("\n");
  free(arr);
}
