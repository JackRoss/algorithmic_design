#include "sort.h"
#include "util.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define LIMIT pow(2, 18)

int main() {
  struct timespec b_time, e_time;
  printf("#size #insertionw #quickw #insertionb #quickb \n");
  for (size_t i = 128; i < LIMIT; i *= 2) {
    printf("%ld ", i);
    int *array1 = (int *)malloc(sizeof(int) * i);
    int *array2 = (int *)malloc(sizeof(int) * i);
    int *array3 = (int *)malloc(sizeof(int) * i);
    int *array4 = (int *)malloc(sizeof(int) * i);

    for (int j = 0; j < i; j++) {
      // insertion sort worst case, all the value in reverse order
      array1[j] = i - j;
      // insertion sort best case, all the value already ordered
      array3[j] = j + 1;
      // for quick sort we have all the value ordered
      // we are just gonna change the pivot choice
      array2[j] = j + 1;
      array4[j] = j + 1;
    }
    // it becomes too slow
    if (i < 131073) {
      clock_gettime(CLOCK_REALTIME, &b_time);
      insert(array1, i);
      clock_gettime(CLOCK_REALTIME, &e_time);
      printf(" %lf", get_ex_time(b_time, e_time));
      // print_array(array1,i);
    }

    // choose the first element as a pivot
    clock_gettime(CLOCK_REALTIME, &b_time);
    quick_sort(array2, 0, i - 1);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf(" %lf", get_ex_time(b_time, e_time));
    // print_array(array2,i);

    clock_gettime(CLOCK_REALTIME, &b_time);
    insert(array3, i);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf(" %lf", get_ex_time(b_time, e_time));
    // print_array(array3,i);

    // choose the element in the middle
    clock_gettime(CLOCK_REALTIME, &b_time);
    quick_sort1(array4, 0, i - 1);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf(" %lf\n", get_ex_time(b_time, e_time));
    // print_array(array4,i);

    free(array1);
    free(array2);
    free(array3);
    free(array4);
  }
}
