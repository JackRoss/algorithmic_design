

#ifndef GRAPH
#define GRAPH

typedef struct {
  int node;   // the sink node
  int weight; // the weight of the edge
} edge;

typedef struct {
  edge **edges;   // an array storing all the edges to a particular node
  int edges_len;  // number of actual edges "occupied"
  int edges_size; // allocated memory for the edges vector (used in the adding
                  // method)
  int dist;       // used to store the actual distance from the source
  int prev;       // the node we go through to get the minimum distance from the
                  // source
  int id;
} node;

typedef struct {
  node **nodes;  // nodes of our graphs
  int nodes_len; //  # of nodes
  int nodes_size;

} graph;

graph *build_graph();

void add_node(graph *g, int i); // called inside the add_edge procedure

void add_edge(graph *g, int a, int b, int w);

void freeGraph(graph *g);

void freeNode(node *n);

graph *Buildrandom(const int HUGE_N, const int MAX_EDGES, const int MAX_WEIGHT);
void print(graph *g, const int HUGE_N);

#endif
