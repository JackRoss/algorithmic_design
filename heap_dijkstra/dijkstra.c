
#include "dijkstra.h"

#include "heap.h"
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

void init(graph *g) {

  for (int i = 0; i < (g->nodes_len); i++) {

    node *current = (g->nodes[i]);
    if (current != NULL) {
      current->dist = 1000;
      current->prev = 0;
    }
  }
}

void print_path(graph *g, int i) {
  int n, j;
  node *v, *u;
  i = i;
  v = g->nodes[i];
  if (v->dist == 1000) {
    printf("no path\n");
    return;
  }
  n = 1;
  u = v;
  while (u->dist > 0) {
    n++;
    u = g->nodes[u->prev];
  }

  int *path = malloc(n * sizeof(int));
  path[n - 1] = i;
  j = 0;

  u = v;
  while (u->dist > 0) {
    path[n - j - 2] = u->prev;
    j++;
    u = g->nodes[u->prev];
  }

  printf("%d ", v->dist);
  printf(" %d ", path[0]);
  for (int i = 1; i < n; i++)
    printf("  -> %d ", path[i]);

  printf("\n");
}

void dijkstra_heap(graph *g, int a, int b) {
  init(g); // WE initialize all the needed values
  int a_i = a;
  int b_i = b;
  node *source = g->nodes[a_i]; // we get the source

  source->dist = 0; // and set its distance to zero

  heap *Queue = BuildHeap(g);

  while (Queue->size > 0) {
    node *u = extract_min(Queue);

    if (u->id == b_i)
      break;

    int n = u->edges_len;

    for (int j = 0; j < n; j++) {

      edge *e = u->edges[j];

      int node_id = e->node;
      node *v = g->nodes[node_id];

      int new_dist = u->dist + e->weight;
      int old_dist = v->dist;

      if (new_dist < old_dist) {

        v->dist = new_dist;
        v->prev = u->id;

        // hswap(Queue, 0, node_id);

        Heapify(Queue, 0);
      }
    }
  }

  print_path(g, b);
}
