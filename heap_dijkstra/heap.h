#ifndef HEAP
#define HEAP

typedef struct {

  node **nodes;
  int size;
  int original_size;
} heap;

int Left(int i);

int Right(int i);
int isValidNode(heap *H, int i);
int isRoot(int i);
int GetRoot(heap *H);
int Parent(int i);

void Heapify(heap *h, int i);
heap *BuildHeap(graph *g);
heap *BuildEmptyHeap();

void hswap(heap *h, int i, int m);

node *extract_min(heap *h);
void RemoveMinimum(heap *h);

void freeHeap(heap *h);

#endif
