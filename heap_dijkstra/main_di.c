#include "dijkstra.h"
#include "graph.h"
#include "heap.h"
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LIMIT pow(2, 14)
double get_ex_time(const struct timespec b_time, const struct timespec e_time) {
  return (e_time.tv_sec - b_time.tv_sec) +
         (e_time.tv_nsec - b_time.tv_nsec) / 1E9;
}

int main() {
  struct timespec b_time, e_time;

  /*  printf("------ TEST FOR GRAPH 1 -------------\n");
    graph *g = build_graph();
    add_edge(g, 0, 1, 7);
    add_edge(g, 0, 2, 9);
    add_edge(g, 0, 4, 14);
    add_edge(g, 1, 2, 10);
    add_edge(g, 1, 3, 15);
    add_edge(g, 2, 3, 11);
    add_edge(g, 2, 5, 2);
    add_edge(g, 3, 4, 6);
    add_edge(g, 4, 5, 9);
    printf(" Expected 11 acf\n");
    printf(" Dijkstra Heap : ");
    dijkstra_heap(g, 0, 5);
      free(g);
  */

  for (size_t i = 8; i <= LIMIT; i *= 2) {

    graph *grafo = Buildrandom(i, i / 3, 20);
    clock_gettime(CLOCK_REALTIME, &b_time);
    dijkstra_heap(grafo, 2, i / 2);
    clock_gettime(CLOCK_REALTIME, &e_time);
    printf("\n%ld", i);

    printf(" %lf\n", get_ex_time(b_time, e_time));

    freeGraph(grafo);
  }
  return 0;
}
