#include "graph.h"
#include "heap.h"
#include <stdio.h>
#include <stdlib.h>

#include <math.h>

void freeHeap(heap *h) {
  for (int i = 0; i < h->original_size; i++)
    freeNode(h->nodes[i]);

  free(h);
}

int Left(int i) { return (2 * i) + 1; }

int Right(int i) { return (2 * i) + 2; }

int Parent(int i) { return floor(i / 2); }

int IsValidNode(heap *h, int i) { return h->size >= i; }

int getRoot() { return 0; }
void hswap2(node **n1, node **n2) {
  node *tmp = *n1;
  *n1 = *n2;
  *n2 = tmp;
}
void hswap1(heap *h, int i, int m) {
  node *tmp = (h->nodes[i]);
  (h->nodes[i]) = (h->nodes[m]);
  (h->nodes[m]) = tmp;
}

node *extract_min(heap *h) {
  node *tmp = h->nodes[0];
  // printf(" node min dist %d\n", tmp -> dist);
  hswap2(&(h->nodes[0]), &(h->nodes[h->size - 1]));
  h->size--;
  Heapify(h, 0);

  return tmp;
}

heap *BuildEmptyHeap() {
  heap *h = (heap *)malloc(sizeof(heap));
  h->nodes = NULL;
  h->size = 0;
  return h;
}

heap *BuildHeap(graph *g) {
  int size = g->nodes_len;
  node **heap_nodes = (node **)malloc(size * sizeof(node *));
  for (int i = 0; i < size; i++) {
    heap_nodes[i] = g->nodes[i];
  }

  heap *h = BuildEmptyHeap();

  h->nodes = heap_nodes;
  h->size = size;
  h->original_size = size;

  Heapify(h, 0);

  return h;
}
void hswap(heap *h, int i, int m) {
  node *tmp = (h->nodes[i]);
  (h->nodes[i]) = (h->nodes[m]);
  (h->nodes[m]) = tmp;
}

void Heapify(heap *h, int i) {
  int left = 2 * i + 1;
  int right = 2 * i + 2;

  int smallest;

  if (left < h->size && h->nodes[left]->dist < h->nodes[i]->dist) {
    smallest = left;
  } else {
    smallest = i;
  }

  if (right < h->size && h->nodes[right]->dist < h->nodes[smallest]->dist) {
    smallest = right;
  }

  if (smallest != i) {
    hswap2(&(h->nodes[i]), &(h->nodes[smallest]));
    Heapify(h, smallest);
  }
}
